# -*- coding: UTF-8 -*-
import time
from threading import Thread
from Queue import Queue
import csv
import os
import re
import urllib
import urllib2
import httplib
import numpy as np
from bs4 import BeautifulSoup
from openpyxl import Workbook
import codecs
import threading

import sys
reload(sys)
sys.setdefaultencoding('utf8')


def run_time(func):
    def wrapper(*args, **kw):
        start = time.time()
        func(*args, **kw)
        end = time.time()
        print('running', end-start, 's')
    return wrapper


def trim(str):
    return re.sub(r"\s", '', str, 0, re.MULTILINE)


class Spider():

    def __init__(self):
        self.qurl = Queue()
        self.data = list()
        self.cookie = None
        self.page_start = int(sys.argv[1])
        self.page_end = int(sys.argv[2])
        self.thread_num = 10
        self.file = None
        self.writer = None
        self.user_agents = ['Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6',
                            'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.12 Safari/535.11',
                            'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)']

    def get_cookie(self):
        url = 'http://femhzs.mofcom.gov.cn/fecpmvc/pages/fem/CorpJWList.html'
        try:
            req = urllib2.Request(
                url, headers={'User-Agent': self.user_agents[1]})
            res = urllib2.urlopen(req)
            _cookie = res.headers.getheader('Set-Cookie')
            regex = r"(Path.*,)|(path=/)|\s"
            self.cookie = re.sub(regex, '', _cookie, 0, re.MULTILINE)
        except (urllib2.HTTPError, urllib2.URLError), e:
            print e

    def produce_url(self):
        baseurl = 'http://femhzs.mofcom.gov.cn/fecpmvc/pages/fem/CorpJWList_nav.pageNoLink.html?session=T&sp={}&sp=S+_t1.CORP_CDE,+_t1.id&sp=T&sp=S'
        for i in range(self.page_start - 1, self.page_end):
            url = baseurl.format(i)
            self.qurl.put(url)

    def get_info(self):
        while not self.qurl.empty():
            url = self.qurl.get()
            sleep_secs = np.random.rand()*3
            time.sleep(sleep_secs)
            print 'crawling {} {} {}'.format(url, threading._get_ident(), sleep_secs)

            try:
                headers = {
                    'User-Agent': self.user_agents[1 % len(self.user_agents)], 'Cookie': self.cookie}
                req = urllib2.Request(url, headers=headers)
                source_html = urllib2.urlopen(req).read()
                plain_text = str(source_html)
            except httplib.BadStatusLine:
                pass
            except (urllib2.HTTPError, urllib2.URLError), e:
                print e
                continue

            soup = BeautifulSoup(plain_text, 'html.parser')
            table_soup = soup.find('table', {'class': 'm-table'})

            for tr in table_soup.find_all('tr'):
                td = tr.find_all('td')
                if(len(td) == 0):
                    continue
                row = [
                    trim(td[0].text),
                    trim(td[1].text),
                    trim(td[2].text)
                ]
                self.writer.writerow(row)
                self.file.flush()
                print ', '.join(row).decode('utf-8')

    def generate_csv(self):
        filepath = 'rows.csv'
        if os.path.exists(filepath):
            os.remove(filepath)
        self.file = open(filepath, "a")
        self.file.write(codecs.BOM_UTF8)
        self.writer = csv.writer(self.file, delimiter=",")
        self.writer.writerow(['境外投资企业(机构)', '境内投资者名称', '投资国别（地区）'])
        self.file.flush()

    @run_time
    def run(self):
        self.get_cookie()
        self.produce_url()
        self.generate_csv()

        ths = []
        for _ in range(self.thread_num):
            th = Thread(target=self.get_info)
            th.start()
            ths.append(th)
        for th in ths:
            th.join()

        print('Data crawling is finished.')


if __name__ == '__main__':
    print "the script is called with %s " % sys.argv
    if len(sys.argv) != 3:
        exit()
    Spider().run()


# thread performance benchmark (page_end: 100)
# 10 ('running', 4.522032976150513, 's')
# 20 ('running', 14.797853946685791, 's')
# 30 ('running', 12.279613018035889, 's')
# 40 ('running', 11.236012935638428, 's')
# 50 ('running', 10.787160158157349, 's')
# 60 ('running', 10.857233047485352, 's')
# 70 ('running', 10.121788024902344, 's')
# 90 ('running', 9.949590921401978, 's')
# 100 ('running', 9.625210046768188, 's')
