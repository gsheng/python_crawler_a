import threading
import os
import time

page_num = 0
mutex = threading.Lock()


def count():
    global page_num

    time.sleep(1)
    page_num += 1
    print('pid %s, %s' % (os.getpid(), page_num))


class MyThread(threading.Thread):
    def run(self):
        global page_num
        time.sleep(1)
        if mutex.acquire(1):  
            page_num += 1
            # msg = self.name + ': num value is ' + str(page_num)
            # print(msg)            
            print('pid %s, thread name %s, thread %s, var %s' % (self.name, os.getpid(), threading._get_ident(), page_num))
            mutex.release()
        
        # time.sleep(1)


if __name__ == "__main__":
    # print dir(threading)
    for i in range(50):
        t = MyThread()
        t.start()
