# -*- coding: UTF-8 -*-
import os
import sys
import time
import datetime
import httplib
import re
import urllib
import urllib2
import requests
import numpy as np
from bs4 import BeautifulSoup
from openpyxl import Workbook
import threading
import logging
import csv

reload(sys)
sys.setdefaultencoding('utf8')

# Some User Agents
user_agents = ['Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6',
               'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.12 Safari/535.11',
               'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)']

start_time = datetime.datetime.now()
# corp_list = []

cookie = None
page_num = 0
writer = None
file = None
lock = threading.Lock()


def corp_spider_a():
    global page_num, lock, cookie, writer, file
    retry_times = 0

    while(1):
        # url='http://femhzs.mofcom.gov.cn/fecpmvc/pages/fem/CorpJWList_nav.pageNoLink.html?session=T&sp=5&sp=S+_t1.CORP_CDE,+_t1.id&sp=T&sp=S' # For Test
        url = 'http://femhzs.mofcom.gov.cn/fecpmvc/pages/fem/CorpJWList_nav.pageNoLink.html?session=T&sp=' + \
            str(page_num) + '&sp=S+_t1.CORP_CDE,+_t1.id&sp=T&sp=S'
        sleep = np.random.rand()*5
        # time.sleep(sleep)

        try:
            headers = {
                'User-Agent': user_agents[1 % len(user_agents)], 'Cookie': cookie}
            req = urllib2.Request(url, headers=headers)
            source_html = urllib2.urlopen(req).read()
            plain_text = str(source_html)
        except httplib.BadStatusLine:
            pass
        except (urllib2.HTTPError, urllib2.URLError), e:
            print e
            continue

        soup = BeautifulSoup(plain_text, 'html.parser')
        table_soup = soup.find('table', {'class': 'm-table'})

        retry_times += 1

        if table_soup == None and try_times < 10:
            continue
        elif table_soup == None or len(table_soup) <= 1 or page_num > 8:
            break  # Break when no informatoin got after 200 times requesting

        for tr in table_soup.find_all('tr'):
            td = tr.find_all('td')
            if(len(td) == 0):
                continue
            td_0 = trim(td[0].text)
            td_1 = trim(td[1].text)
            td_2 = trim(td[2].text)
            row = [td_0, td_1, td_2]
            # corp_list.append(row)

            writer.writerow(row)
            time.sleep(1)
            file.flush()

            print ', '.join(row).decode('utf-8')
            retry_times = 0  # set 0 when got valid information
        print('>>>>> Downloading Information From Page %d Seconds: %s process id: %s' % (
            page_num, (datetime.datetime.now() - start_time).seconds, os.getpid()))
        with lock:
            page_num += 1
            pass

# def generate_excel(corp_list):
#     wb = Workbook(write_only=True)
#     ws = []
#     ws.append(wb.create_sheet(title='Sheet1'))
#     ws[0].append(['境外投资企业(机构)', '境内投资者名称', '投资国别（地区）'])
#     for td in corp_list:
#         ws[0].append([td[0], td[1], td[2]])
#     wb.save('corp_data.xlsx')


def corp_spider():
    global page_num, lock
    while(page_num < 100):
        time.sleep(1)
        print '{} {} {}'.format(page_num, os.getpid(), threading._get_ident())
        with lock:
            page_num += 1            


def get_cookie():
    url = 'http://femhzs.mofcom.gov.cn/fecpmvc/pages/fem/CorpJWList.html'
    try:
        req = urllib2.Request(url, headers={'User-Agent': user_agents[1]})
        res = urllib2.urlopen(req)
        # print dir(res)
        _cookie = res.headers.getheader('Set-Cookie')
        regex = r"(Path.*,)|(path=/)|\s"
        cookie = re.sub(regex, '', _cookie, 0, re.MULTILINE)
        print cookie
        return cookie
    except (urllib2.HTTPError, urllib2.URLError), e:
        print e


def trim(str):
    return re.sub(r"\s", '', str, 0, re.MULTILINE)


class MyThread(threading.Thread):
    def run(self):
        corp_spider()

if __name__ == '__main__':
    print 'hello world'
    global cookie
    # cookie = get_cookie()

    # with open("rows.csv", "a") as file:
    #     writer = csv.writer(file, delimiter=",")
    #     writer.writerow(["mark", "123"])
    #     time.sleep(1)
    #     file.flush()

    file = open("rows.csv", "a")
    writer = csv.writer(file, delimiter=",")
    writer.writerow(['境外投资企业(机构)', '境内投资者名称', '投资国别（地区）'])
    time.sleep(1)
    file.flush()

    for i in range(10):
        t = MyThread()
        t.start()
