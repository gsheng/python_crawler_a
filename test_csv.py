import csv
import time

with open("rows.csv", "a") as file:
    writer = csv.writer(file, delimiter=",")

    end = time.time() + 100
    while True:
        if time.time() > end:
            break
        else:
            writer.writerow(["mark", "123"])
            time.sleep(1)
            file.flush()
