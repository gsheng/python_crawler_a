# -*- coding: UTF-8 -*-
import os
import sys
import time
import datetime
from openpyxl import Workbook


 


reload(sys)
sys.setdefaultencoding('utf8')

filepath = 'corp_data.xlsx'

def generate_excel():
    global filepath
    wb = Workbook(write_only=True)
    ws = []
    ws.append(wb.create_sheet(title='Sheet1'))
    ws[0].append(['境外投资企业(机构)', '境内投资者名称', '投资国别（地区）'])
    # for td in corp_list:
    #     ws[0].append([td[0], td[1], td[2]])
    wb.save(filepath)

def append_to_excel(corp_list):
    global filepath
    wb=Workbook()
    sheet=wb.active
    data=[('Id','Name','Marks'),(1,'ABC',50),(2, 'CDE' ,100)]

    for row in data:
        sheet.append(row)
    wb.save(filepath)

if __name__ == '__main__':
    print 'hello'
    generate_excel()
    corp_list = [[1,2,3],[4,5,6]]
    append_to_excel(corp_list)