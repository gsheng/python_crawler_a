# -*- coding: UTF-8 -*-
import os
import sys
import time
import datetime
import httplib
import re
import urllib
import urllib2
import requests
import numpy as np
from bs4 import BeautifulSoup
from openpyxl import Workbook
import threading
import logging
import csv
import inspect
from threading import Thread

reload(sys)
sys.setdefaultencoding('utf8')

start_time = datetime.datetime.now()


page_num = 0
lock = threading.Lock()


def corp_spider():
    global page_num

    while(page_num < 20):
        print '{} {} {}'.format(page_num, os.getpid(), threading._get_ident())
        with lock:
            page_num += 1
            time.sleep(1)


# class Thread(threading.Thread):
#     def __init__(self, t, *args):
#         threading.Thread.__init__(self, target=t, args=args)
#         self.start()


if __name__ == '__main__':
    ths = []
    for _ in range(10):
        th = Thread(target=corp_spider)
        th.start()
        ths.append(th)
    # for th in ths:
    #     th.join()
