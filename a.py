
import time
import requests
import multiprocessing 
from multiprocessing import Pool 
from bs4 import BeautifulSoup

MAX_WORKER_NUM = multiprocessing.cpu_count()

t1 = time.time()

urls = ['<https://movie.douban.com/top250?start={}&filter=>'.format(i) for i in range(0, 226, 25)]

print urls

def job(url):
    print(time.time())
    # print dir(requests)
    r = requests.get(url)
    print 'dir(r)'
    # content = r.text
    # print(r)
    # soup = BeautifulSoup(content, 'html.parser')
    # item = soup.select(".item")
    # for i in item:
    #     print(i.select(".title")[0].text.decode('utf-8'))

p = Pool(MAX_WORKER_NUM)
for url in urls:
    p.apply_async(job, args=(url,))
p.close()
p.join()
print("time: ", time.time()-t1)
